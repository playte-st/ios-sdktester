# Playtest iOS SDK + Tester (1.1.8) #

This is the Playtest iOS SDK and Tester for the Playte.st iOS (version 1.1). The SDK has been tested with iOS7 to iOS9.2

### Just need the SDK ###
* Clone this repo or download as zip
* Please find the latest version under "Library"
* The static library contains 3 files (PTConnect.h, libPTConnect.a, PTResource.bundle). 

### I want to play with the SDK and Tester app ###
Before using the SDK or tester, you need to setup a studio account and a product. 

1. Get a playte.st or dofeedback.io studio account (https://www.playte.st/studios / https://www.dofeedback.io/studios)
2. Create a game
3. Get your API key and Playtest ID
4. Clone this repo or download as zip
5. (OPTIONAL) Download the iOS Companion app (https://itunes.apple.com/us/app/playtest-test-play-early-prototypes/id947418882?mt=8)
6. (OPTIONAL) Login with your studio credentials. Once you're logged in, we need to register your device in order for you to validate the SDK tester (very important!)
7. Run the SDKTester on your iPhone or iPad

If everything is setup correctly, the app validation will be successful and your ready to use the SDK Tester.

### Any questions? ###

* Questions? Check the online documentation for the SDK at the studio online portal
* Send us an email at hello (at) playte.st

### Changelog ###


09/30/15 - Updated SDK Tester to support iOS 9.0.1

10/10/15 - Updated SDK Tester to support iOS 9.2.1

03/04/16 - Revamped SDK to version 1.1

04/16/16 - Universal SDK, supports iOS 9.3.1 and simulator.

05/20/16 - Updated SDK to Support Universal, and made validation upfront login possible

09/14/16 - Updated SDK to Support limited IDFA

10/11/17 - Updated SDK to Support iOS 11

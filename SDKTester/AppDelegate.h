//
//  AppDelegate.h
//  SDKTester
//
//  Created by Bart van den Berg on 07-09-15.
//  Copyright (c) 2015 Bart van den Berg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


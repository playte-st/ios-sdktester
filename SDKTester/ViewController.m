//
//  ViewController.m
//  SDKTester
//
//  Created by Bart van den Berg on 07-09-15.
//  Copyright (c) 2015 Bart van den Berg. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPresentFeedback) name:PTDidPresentFeedbackNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didDismissFeedback) name:PTDidDismissFeedbackNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didUpdateUserInformation) name:PTDidReceiveUserUpdateNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark notification methods
-(void)didPresentFeedback
{
    // When feedback screen is displayed
    NSLog(@"Present feedback screen");
}

-(void)didDismissFeedback
{
   // When feedback screen is dismissed
    NSLog(@"Dismissed feedback screen");
    
    // Manually restore if required
    [[PTConnect manager]setEngagementViewHidden:NO];
}

-(void)didUpdateUserInformation
{
    NSLog(@"User Avatar URL: %@",[[PTConnect manager]avatarURL]);
    NSLog(@"First name: %@",[[PTConnect manager]firstName]);
    NSLog(@"Last name: %@",[[PTConnect manager]lastName]);
    NSLog(@"Display name: %@",[[PTConnect manager]displayName]);
    
    [self.nameLabel setText:[[PTConnect manager]displayName]];
    [self.avatarImageView setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[PTConnect manager]avatarURL]]]]];
}

- (IBAction)doSendEventData:(id)sender
{
 
    // Example mark checkpoint. Screenshot and current event data is NSDictionary. Completion block can be used to notify users if desirable.
    [[PTConnect manager] markCheckpoint:@"level_3_completed" withScreenshot:YES withEventData:[[PTConnect manager] eventData] onCompletion:^(NSData *responseData, NSError *error)
     {
         if(!error)
         {
             NSLog(@"Succesfully stored data, let's continue the app...");
             
             /* Read the returning data if you want. 
             NSError *jsonError;
             NSDictionary *jsonObject;
             
             @try
             {
                 jsonObject = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:&jsonError];
             }
             @catch (NSException *exception)
             {
                 NSLog(@"Error: %@",[exception description]);
             }
             if(jsonObject)
             {
                 NSLog(@"JSON Response: %@",[jsonObject description]);
             }
              */
         }
         else
         {
             NSLog(@"Unable to store checkpoint %@",[error localizedDescription]);
         }
         
     }];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PTDidPresentFeedbackNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PTDidDismissFeedbackNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PTDidReceiveUserUpdateNotification object:nil];
}
@end

//
//  AppDelegate.m
//  SDKTester
//
//  Created by Bart van den Berg on 07-09-15.
//  Copyright (c) 2015 Bart van den Berg. All rights reserved.
//

#import "AppDelegate.h"
#import "PTConnect.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    
    // OPTIONAL: Set content type to match your product. This is important, because the SDK uses different ways to gather stats and render screenshots. Default PTContetTypeApp
    [[PTConnect manager] setContenttype:PTContentTypeGame];
    
    // OPTIONAL: If you want your app to have a kill switch. Will check if user is known in the system and should have access to the build. Default NO
    [[PTConnect manager] setNeedsValidation:NO];
    
    // OPTIONAL: Set the way how to trigger feedback this can be disabled three, four, five finger or shake. Default not set
    [[PTConnect manager] setFeedbackmethod:PTFeedbackShake];
    
    // OPTIONAL: Set log level to none, debug or verbose. Default PTLogLevelNone
    [[PTConnect manager] setLogLevel:PTLogLevelDebug];
    
    // OPTIONAL: If you want the message center manually enabled or disabled. Default NO
    [[PTConnect manager] setEngagementViewHidden:NO];
    
    
    // Fill in your API Key & Playtest ID (both are displayed under your account under game -> results
    [[PTConnect manager] setupFeedbackID:@"" withApiKey:@""];

    
    // Example: Sending the current version as event data
    NSString *bundle = [[NSBundle mainBundle] bundleIdentifier];
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString *build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    
    [[PTConnect manager] setEventData:@{@"Bundle" : bundle, @"Version":version, @"Build":build}];

    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end

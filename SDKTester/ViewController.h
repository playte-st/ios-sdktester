//
//  ViewController.h
//  SDKTester
//
//  Created by Bart van den Berg on 07-09-15.
//  Copyright (c) 2015 Bart van den Berg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PTConnect.h"

@interface ViewController : UIViewController
- (IBAction)doSendEventData:(id)sender;
@property (strong, nonatomic) UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;


@end

